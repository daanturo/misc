#include <iostream>
#include <stdio.h>
#include <string.h>
#include <string>

using namespace std;

class DSAException {
  int _error;
  string _text;

public:
  DSAException() : _error(0), _text("Success") {}
  DSAException(int err) : _error(err), _text("Unknown Error") {}
  DSAException(int err, const char *text) : _error(err), _text(text) {}

  int getError() { return _error; }
  string &getErrorText() { return _text; }
};

template <class T> struct L1Item {
  T data;
  L1Item<T> *pNext;
  L1Item() : pNext(NULL) {}
  L1Item(T &a) : data(a), pNext(NULL) {}
};

template <class T> class L1List {
  L1Item<T> *_pHead; // The head pointer of linked list
  size_t _size;      // number of elements in this list
public:
  L1List() : _pHead(NULL), _size(0) {}
  ~L1List();

  void clean();
  bool isEmpty() { return _pHead == NULL; }
  size_t getSize() { return _size; }

  T &at(int i);         // give the reference to the element i-th in the list
  T &operator[](int i); // give the reference to the element i-th in the list

  bool find(T &a, int &idx); // find an element similar to a in the list. Set
                             // the found index to idx, set idx to -1 if failed.
                             // Return true if success.
  int insert(int i, T &a);   // insert an element into the list at location i.
                             // Return 0 if success, -1 otherwise
  int remove(int i); // remove an element at position i in the list. Return 0 if
                     // success, -1 otherwise.

  int push_back(T &a);  // insert to the end of the list
  int insertHead(T &a); // insert to the beginning of the list

  int removeHead(); // remove the beginning element of the list
  int removeLast(); // remove the last element of the list

  void reverse();

  void traverse(void (*op)(T &)) {
    // TODO: Your code goes here
  }
  void traverse(void (*op)(T &, void *), void *pParam) {
    struct TPointerHolder {
      L1Item<T> *Pointer;
    };
    static_cast<TPointerHolder *>(pParam)->Pointer = this->_pHead;
  }
    void print() {
    L1Item<T> *p = this->_pHead;
    while (p){
      cout<<p->data<<endl;
      p = p->pNext;
    }
  }
};

/// Insert item to the end of the list
/// Return 0 if success, -1 otherwise
template <class T> int L1List<T>::push_back(T &a) {
  // TODO: Your code goes here
  int dummyIndex = 0;
  bool duplicated = find(a, dummyIndex);
  if (duplicated)
    return -1;
  if (this->isEmpty()) {
    insertHead(a);
    return 0;
  }
  L1Item<T> *p = this->_pHead;
  L1Item<T> *res = new L1Item<T>(a);
  if (res == nullptr) {
    delete res;
    return -1;
  }
  while (p->pNext /*!= nullptr*/) {
    if (p->data == a) {
      delete res;
      return -1;
    }
    p = p->pNext;
  }
  p->pNext = res;
  this->_size++;
  return 0;
}
template <class T> bool L1List<T>::find(T &a, int &idx) {
  if (isEmpty())
    return false;
  L1Item<T> *p = this->_pHead;
  idx = -1;
  while (p) {
    idx++;
    if (p->data == a) {
      return true;
    }
    p = p->pNext;
  }
  // reach the end of list but still not found
  idx = -1;
  return false;
}
/// Insert item to the front of the list
/// Return 0 if success, -1 otherwise
template <class T> int L1List<T>::insertHead(T &a) {
  // TODO: Your code goes here
  //   int dummyIndex = 0;
  //   bool duplicated = find(a, dummyIndex);
  //   if (duplicated)
  //     return -1;
  L1Item<T> *p = new L1Item<T>(a);
  if (p == nullptr) {
    delete p;
    return -1;
  }
  p->pNext = this->_pHead;
  this->_size++;
  this->_pHead = p;
  return 0;
}

/// Remove the first item of the list
/// Return 0 if success, -1 otherwise
template <class T> int L1List<T>::removeHead() {
  // TODO: Your code goes here
  if (isEmpty())
    return -1;
  L1Item<T> *next = _pHead->pNext;
  delete this->_pHead;
  this->_pHead = next;
  this->_size--;
  return 0;
}

/// Remove the last item of the list
/// Return 0 if success, -1 otherwise
template <class T> int L1List<T>::removeLast() {
  // TODO: Your code goes here
  if (isEmpty())
    return -1;
  if (this->_size == 1) {
    this->removeHead();
    return 0;
  }
  L1Item<T> *pPre = this->_pHead;
  // forward until pPre->pNext is the last link
  while (pPre->pNext->pNext != nullptr)
    pPre = pPre->next;
  delete pPre->pNext;
  pPre->PNext = nullptr;
  this->_size--;
  return 0;
}
template <class T> void L1List<T>::clean() {
  L1Item<T> *temp = nullptr;
  while (this->_pHead != nullptr) {
    temp = this->_pHead;
    this->_pHead = this->_pHead->pNext;
    delete temp;
    _size--;
  }
  _size = 0;
}

template <class T> L1List<T>::~L1List<T>() { this->clean(); }

template <class T> T &L1List<T>::operator[](int i) {
  L1Item<T> *p = this->_pHead;
  if (i >= this->_size || i < 0 || isEmpty())
    return p->data;
  while (i > 0) { // decrease until i==0
    p = p->pNext;
    i--;
  }
  return p->data;
}
template <class T> void L1List<T>::reverse() {
  //if (_size < 2)
    //return;
  L1Item<T> *newHead = nullptr;
  L1Item<T> *pIterator = _pHead;
  L1Item<T> *chain = nullptr;
  while (pIterator) {
    newHead = pIterator;
    pIterator = pIterator->pNext;
    newHead->pNext = chain;
    chain = newHead;
  }
  _pHead = newHead;
}
template <class T> int L1List<T>::insert(int i, T &a) {
  // insert an element into the list at location i.
  // Return 0 if success, -1 otherwise

  int dummyIndex = 0; // Check duplication
  bool duplicated = find(a, dummyIndex);
  if (duplicated)
    return -1;

  if (isEmpty() || i == 0) {
    insertHead(a);
    return 0;
  }
  if (i >= _size || i < 0) {
    return -1;
  }
  L1Item<T> *pPre = _pHead;
  while (i > 0) {
    pPre = pPre->pNext;
    i--;
  }
  L1Item<T> *pFormerNext = pPre->pNext;
  pPre->pNext = new L1Item<T>(a);

  if (pPre->pNext == nullptr) { // if allocation failed
    pPre->pNext = pFormerNext;
    return -1;
  }

  pPre->pNext->pNext = pFormerNext;
  _size++;
  return 0;
}
template <typename T> int L1List<T>::remove(int i) {
  // remove an element at position i in the list. Return 0 if
  // success, -1 otherwise.
  if (i >= _size || i < 0)
    return -1;
  if (i == 0) {
    removeHead();
    return 0;
  }
  L1Item<T> *pPre = _pHead;
  while (i > 1) {
    pPre = pPre->pNext;
    i--;
  }
  L1Item<T> *pNextNext = pPre->pNext->pNext;
  delete pPre->pNext;
  pPre->pNext = pNextNext;
  _size--;
  return 0;
}
int main(){
  cout<<(4|-3);
}
