// http://www.cplusplus.com/doc/tutorial/inheritance/
// multiple inheritance
#include <iostream>
#include <fstream>

using namespace std;

//
class UnitCell {
  protected: int value;
  public:
    UnitCell(int v) {
      value = 0;
      if (checkValidation(v)) value = v;
	} 
	~UnitCell(){
	}
    
	bool checkValidation(int v){ 
	  if ((v>=1)&&(v<=9))
	     return true; 
	  return false;
	}
	
	bool setValue(int v){
	  if (checkValidation(v)) value = v;
	  else return false;
	  return true;
	}
	
	int getValue(){
	  return value;
	}
	
};

// ma tran vuong 3x3
class UnitMatrix {
  protected:
    UnitCell cell;
  public: 
	bool checkValidation(){
	  return false;
	}
};

// ma tran vuong 9x9
class SudokuMatrix {
  protected:
    UnitMatrix umat;
  public:
    bool checkValidation(){
	  return false;
	}
	void print(){ 
	
	}
	bool update(int x, int y, int val){ 
		return true;
	}
};


///////////////////////////////
//  main funtion
void readInputfile(const char* filename, SudokuMatrix sudo);
int main () {
  SudokuMatrix sudo;
  readInputfile("sample0002.txt", sudo);
  
  return 0;
}


///////////////////////////////
//  supplementary funtions
void readInputfile(const char* filename, SudokuMatrix sudo){
  string line;
  ifstream myfile (filename);
  if (myfile.is_open())
  {
    while ( getline (myfile,line) )
    {
      cout << line << '\n';
    }
    myfile.close();
  }

  else cout << "Unable to open file"; 
}

