y = bin(2000)
m = bin(12)
d = bin(31)

ymdS = y[2:] + m[2:] + d[2:]
dmyS = d[2:] + m[2:] + y[2:]

dmy = 0
ymd = 0

for bit in ymdS:
    ymd = ymd*2 + int(bit)
for bit in dmyS:
    dmy = dmy*2 + int(bit)
    
print("%x\n%x\n%x\n%x\n%x" % (ymd,dmy,2000,31,12))