#include <iostream>
#include <stdio.h>
#include <string.h>
#include <string>

using namespace std;

class DSAException {
  int _error;
  string _text;

public:
  DSAException() : _error(0), _text("Success") {}
  DSAException(int err) : _error(err), _text("Unknown Error") {}
  DSAException(int err, const char *text) : _error(err), _text(text) {}

  int getError() { return _error; }
  string &getErrorText() { return _text; }
};

template <class T> struct L1Item {
  T data;
  L1Item<T> *pNext;
  L1Item() : pNext(NULL) {}
  L1Item(T &a) : data(a), pNext(NULL) {}
};

template <class T> class L1List {
  L1Item<T> *_pHead; // The head pointer of linked list
  size_t _size;      // number of elements in this list
public:
  L1List() : _pHead(NULL), _size(0) {}
  ~L1List();

  void clean();
  bool isEmpty() { return _pHead == NULL; }
  size_t getSize() { return _size; }

  T &at(int i);         // give the reference to the element i-th in the list
  T &operator[](int i); // give the reference to the element i-th in the list

  bool find(T &a, int &idx); // find an element similar to a in the list. Set
                             // the found index to idx, set idx to -1 if failed.
                             // Return true if success.
  int insert(int i, T &a);   // insert an element into the list at location i.
                             // Return 0 if success, -1 otherwise
  int remove(int i); // remove an element at position i in the list. Return 0 if
                     // success, -1 otherwise.

  int push_back(T &a);  // insert to the end of the list
  int insertHead(T &a); // insert to the beginning of the list

  //  L1Item<T> *pushLastPos(T, L1Item<T> *&);
  void pushLastPos(T, L1Item<T> *&);

  int removeHead(); // remove the beginning element of the list
  int removeLast(); // remove the last element of the list

  void reverse();

  void traverse(void (*op)(T &)) {
    // TODO: Your code goes here
  }
  void traverse(void (*op)(T &, void *), void *pParam) {}
  bool findIDofCity(int &output, string name) {
    L1Item<T> *p = this->_pHead;
    while (p != nullptr) {
      if (p->data.name == name) {
        output = p->data.id;
        return true;
      }
      p = p->pNext;
    }
    return false;
  }
  //  L1Item<T> *getHead() { return this->_pHead; }
  void print() {
    L1Item<T> *p = _pHead;
    while (p != nullptr) {
      cout << p->data << endl;
      p = p->pNext;
    }
  }
};

/// Insert item to the end of the list
/// Return 0 if success, -1 otherwise
template <class T> int L1List<T>::push_back(T &a) {
  // TODO: Your code goes here
  if (&a == nullptr)
    return -1;
  if (this->_size == 0) {
    insertHead(a);
    return 0;
  }
  L1Item<T> *p = this->_pHead;
  while (p->pNext != nullptr)
    p = p->pNext;
  L1Item<T> *res = new L1Item<T>(a);
  res->pNext = nullptr;
  p->pNext = res;
  this->_size++;
  return 0;
}

/// Insert item to the front of the list
/// Return 0 if success, -1 otherwise
template <class T> int L1List<T>::insertHead(T &a) {
  // TODO: Your code goes here
  L1Item<T> *p = new L1Item<T>(a);
  p->pNext = this->_pHead;
  this->_size++;
  this->_pHead = p;
  return 0;
}

/// Remove the first item of the list
/// Return 0 if success, -1 otherwise
template <class T> int L1List<T>::removeHead() {
  // TODO: Your code goes here
  if (this->_size == 0)
    return -1;
  T *next = _pHead->pNext;
  this->_pHead->pNext = nullptr;
  delete this->_pHead;
  this->pHead = next;
  this->_size--;
  return 0;
}

/// Remove the last item of the list
/// Return 0 if success, -1 otherwise
template <class T> int L1List<T>::removeLast() {
  // TODO: Your code goes here
  if (this->size == 0)
    return -1;
  if (this->_size == 1) {
    this->removeHead();
    return 0;
  }
  T *pPre = this->_pHead;
  while (pPre->pNext->pNext != nullptr)
    pPre = pPre->next;
  delete pPre->pNext;
  pPre->PNext = nullptr;
  this->_size--;
  return 0;
}
template <class T> void L1List<T>::clean() {
  L1Item<T> *temp;
  while (this->_pHead != nullptr) {
    temp = this->_pHead;
    this->_pHead = this->_pHead->pNext;
    delete temp;
    _size--;
  }
  _size = 0;
}
template <class T> L1List<T>::~L1List<T>() { this->clean(); }
template <class T> T &L1List<T>::operator[](int i) {
  L1Item<T> *p = this->_pHead;
  if (i >= this->_size || i < 0 || this->_size == 0)
    return p->data;
  while (i > 0) {
    p = p->pNext;
    i--;
  }
  return p->data;
}
template <class T> bool L1List<T>::find(T &a, int &idx) {
  T *p = this->head;
  if (p == nullptr) {
    idx = -1;
    return false;
  }
  for (int i = 0; i < _size; i++) {
    if (p->data == a->data) {
      idx = i;
      return true;
    }
    if (p->pNext != nullptr)
      break;
  }
  idx = -1;
  return false;
}
//template <class T>
// L1Item<T> *L1List<T>::pushLastPos(T obj, L1Item<T> *&tail) {
//  if (tail == nullptr) {
//    this->insertHead(obj);
//    return this->_pHead;
//  }
//  L1Item<T> *last = new L1Item<T>(obj);
//  tail->pNext = last;
//  this->_size++;
//  return last;
//}
template <class T>
void L1List<T>::pushLastPos(T obj, L1Item<T> *&tail) {
  if (tail == nullptr) {
    this->insertHead(obj);
    tail = this->_pHead;
    return;
  }
  L1Item<T> *last = new L1Item<T>(obj);
  tail->pNext = last;
  this->_size++;
  tail = last;
}
template <class T> void L1List<T>::reverse() {
  if (_size <= 2)
    return;
  L1Item<T> *pIterator = _pHead;
  L1Item<T> *newHead = nullptr;
  L1Item<T> *newNode = new L1Item<T>;
  while (pIterator != nullptr) {
    newNode->data = pIterator->data;
    newNode->pNext = newHead;
    newHead = newNode;
    pIterator = pIterator->pNext;
  }
  _pHead = newHead;
}

int main() {
  L1List<int> list;
  int num=1;
  list.insertHead(num);
  num++;
  list.insertHead(num);
  num++;
  list.insertHead(num);
  num++;
  list.insertHead(num);
  list.print();
  list.reverse();
//  list.print();
}
