#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

class fact {
  vector <int> v;
  int size;
  const int digits_per_group = 3;
  const int range = (pow(10,digits_per_group));
public:
  fact(){
    v.push_back(1);
    size = 1;
  }
  void print(int in) {
    calc(in);
    cout<<v[size-1]<<" ";
    for (int i=size-2; i>=0; i--) {
      string s = to_string(v[i]);
      while (s.size()< digits_per_group) s = "0"+s;
      cout<<s<<" ";
    }
    cout<<endl;
  };
  void remod(){
    for (int i=0; i<size; i++) {
      if (v[i] >= range) {
        int carry = v[i] / range;
        v[i] = v[i] % range;
        if (i==size-1) {
          v.push_back(0);
          size++;
        }
        v[i+1] +=carry;
      }
    }
  };
  void multiply(int n) {
    for (int i=0; i<size;i++) v[i] *= n;
    remod();
  };
  void calc(int in) {
    v.clear();
    v.push_back(1);
    size = 1;
    for (int i=2 ;i<=in;i++ ) {
      multiply(i);
      cout<<i<<" ";
    }
    cout<<"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
  }
};

int main(int argc, char **argv) {
  fact num;
  while (true) {
    int in;
    cin>>in;
    num.print(in);
  }
  return 0;
}
