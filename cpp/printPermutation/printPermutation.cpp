#include <iostream>

using namespace std;

void printPer(string in, string out=""){
    if (in=="")
        cout<<out<<endl;
    else{
        int i=0;
        int len = in.size();
        for (i=0;i<len;i++){
            string newout=out+in[i];
            string newin=in.erase(i,1);
            newin.erase(i,1);
            printPer(newin,newout);
        }
    }
}

int main(){
    string input;
    cin>>input;
    printPer(input);
}
