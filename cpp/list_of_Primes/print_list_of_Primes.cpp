#include <iostream>
#include <iomanip>
using namespace std;


bool isFibonacci(int i, int Fibo1=1, int Fibo2=1) {
    if (Fibo2>i)
        return false;
    else if (i==Fibo2)
        return true;
    else
        return isFibonacci(i,Fibo2,Fibo1+Fibo2);
}
bool isPrime(int input) {
    if (input<=1)
        return false;
    else {
        for (int i=2; i<=input/2; i++)
            if (input % i ==0)
                return false;
    }
    return true;
}
int main() {
	int k=0;
	for (int i=0;i<1070;i++)
		if (isPrime(i))
			{
				cout<<setw(6)<<i;
				k++;
				if (k % 20 ==0 )
					cout<<endl;
			}
   return 0;
}
